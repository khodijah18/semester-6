#%%
import numpy as np
import skfuzzy as fuzz
from skfuzzy import control as ctrl
#%%
# Variabel input
jenis_hama = ctrl.Antecedent(np.arange(0, 11, 1), 'jenis_hama')
kondisi_cuaca = ctrl.Antecedent(np.arange(0, 11, 1), 'kondisi_cuaca')
jenis_tanaman = ctrl.Antecedent(np.arange(0, 11, 1), 'jenis_tanaman')

# Variabel output
dosis_pestisida = ctrl.Consequent(np.arange(0, 26, 1), 'dosis_pestisida')
#%%
# Fungsi keanggotaan untuk variabel input jenis_hama
jenis_hama['low'] = fuzz.trimf(jenis_hama.universe, [0, 0, 5])
jenis_hama['medium'] = fuzz.trimf(jenis_hama.universe, [0, 5, 10])
jenis_hama['high'] = fuzz.trimf(jenis_hama.universe, [5, 10, 10])
jenis_hama['low'].view()
#%%
# Fungsi keanggotaan untuk variabel input kondisi_cuaca
kondisi_cuaca['sunny'] = fuzz.trimf(kondisi_cuaca.universe, [0, 0, 5])
kondisi_cuaca['cloudy'] = fuzz.trimf(kondisi_cuaca.universe, [0, 5, 10])
kondisi_cuaca['rainy'] = fuzz.trimf(kondisi_cuaca.universe, [5, 10, 10])
kondisi_cuaca['sunny'].view()
#%%
# Fungsi keanggotaan untuk variabel input jenis_tanaman
jenis_tanaman['corn'] = fuzz.trimf(jenis_tanaman.universe, [0, 0, 5])
jenis_tanaman['soybean'] = fuzz.trimf(jenis_tanaman.universe, [0, 5, 10])
jenis_tanaman['wheat'] = fuzz.trimf(jenis_tanaman.universe, [5, 10, 10])
jenis_tanaman['corn'].view()
#%%
# Fungsi keanggotaan untuk variabel output dosis_pestisida
dosis_pestisida['low'] = fuzz.trimf(dosis_pestisida.universe, [0, 0, 8])
dosis_pestisida['medium'] = fuzz.trimf(dosis_pestisida.universe, [0, 8, 16])
dosis_pestisida['high'] = fuzz.trimf(dosis_pestisida.universe, [8, 16, 25])
dosis_pestisida['low'].view()
#%%
# Aturan fuzzy (direpresentasikan dengan 3 rule)
rule1 = ctrl.Rule(jenis_hama['low'] & kondisi_cuaca['sunny'] & jenis_tanaman['corn'], dosis_pestisida['low'])
rule2 = ctrl.Rule(jenis_hama['medium'] & kondisi_cuaca['cloudy'] & jenis_tanaman['soybean'], dosis_pestisida['medium'])
rule3 = ctrl.Rule(jenis_hama['high'] & kondisi_cuaca['rainy'] & jenis_tanaman['wheat'], dosis_pestisida['high'])
rule1.view()
#%%
# Menjalankan sistem inferensi fuzzy
pest_control = ctrl.ControlSystem([rule1, rule2, rule3])
pest_control_simulation = ctrl.ControlSystemSimulation(pest_control)
#%%
# Memasukkan input (contoh)
pest_control_simulation.input['jenis_hama'] = 8
pest_control_simulation.input['kondisi_cuaca'] = 4
pest_control_simulation.input['jenis_tanaman'] = 7
#%%
# Melakukan perhitungan inferensi
pest_control_simulation.compute()
#%%
# Mengambil output
output = pest_control_simulation.output['dosis_pestisida']
print("Hasil : ", output)
#%%
# Melakukan defuzzifikasi
# defuzzified_output = fuzz.defuzz(jenis_hama.universe, pest_control_simulation.output['dosis_pestisida'], 'centroid')

# Menampilkan hasil defuzzifikasi
# print("Pestisida yang optimal untuk pengendalian hama tanaman: ", defuzzified_output)