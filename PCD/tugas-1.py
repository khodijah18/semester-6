import cv2

img = cv2.imread("semester-6/PCD/shuhua.png", 0)

h, w, ch = img.shape
flip_img = img[::-1, ::-1]

print('array citra semula:', img)
print('array citra setelah:', flip_img)
cv2.imshow("citra semula", img)
cv2.imshow("citra setelah", flip_img)
cv2.waitKey(0)

