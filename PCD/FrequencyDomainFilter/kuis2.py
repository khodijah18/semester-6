# import library untuk melakukan smoothing. pada python bisa menggunakan open cv
import cv2

# Membaca gambar
img = cv2.imread("PCD/FrequencyDomainFilter/daun.png")

# Mendefinisikan ukuran kernel dan standard deviation untuk blurring
kernel_size = (5, 5)
sigma = 0

# Menerapkan Gaussian blurring
blurred_img = cv2.GaussianBlur(img, kernel_size, sigma)

# Menampilkan gambar asli dan gambar yang sudah di blur
cv2.imshow("Gambar Asli", img)
cv2.imshow("Gambar setelah smoothing", blurred_img)

# Menunggu input dari user
cv2.waitKey(0)

# Menutup semua jendela
cv2.destroyAllWindows()



# #MEAN

# # Tentukan ukuran kernel
# kernel_size = 5

# # Buat kernel rata-rata
# kernel = np.ones((kernel_size, kernel_size), np.float32) / (kernel_size**2)

# # Lakukan operasi konvolusi dengan kernel
# smoothed_img = cv2.filter2D(img, -1, kernel)

# # Tampilkan hasil smoothing
# cv2.imshow('Smoothed Image', smoothed_img)
# cv2.waitKey(0)
# cv2.destroyAllWindows()

