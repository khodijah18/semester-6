# Libraries
import cv2
import numpy as np
import matplotlib.pyplot as plt

# Open the image
f = cv2.imread("PCD/FrequencyDomainFilter/rontgen.jpeg",0)

# Transform image into frequency domain and shifted
F = np.fft.fft2(f)
Fshift = np.fft.fftshift(F)

plt.imshow(np.log1p(np.abs(Fshift)), cmap='gray')
plt.axis('off')
# plt.show() 

## CREATE BUTTERWORTH LOW PASS FILTER
M,N = f.shape
H = np.zeros((M,N), dtype=np.float32)
D0 = 10 # cut of frequency
n = 10 # order 
for u in range(M):
    for v in range(N):
        D = np.sqrt((u-M/2)**2 + (v-N/2)**2)
        H[u,v] = 1 / (1 + (D/D0)**n)
        
plt.imshow(H, cmap='gray')
plt.axis('off')
# plt.show()

Gshift = Fshift * H # Aplly Butterworth Low Pass Filter to frequency domain image 
G = np.fft.ifftshift(Gshift) # Return back into the corner
g = np.abs(np.fft.ifft2(G)) # Invers fourier transform

plt.imshow(g, cmap='gray')
plt.axis('off')
plt.show() # Result image smoothing with Butterworth Low Pass Filter

##CREATE BUTTERWORTH HIGH PASS FILTER
HPF = np.zeros((M,N), dtype=np.float32)
D0 = 10
n = 1
for u in range(M):
    for v in range(N):
        D = np.sqrt((u-M/2)**2 + (v-N/2)**2)
        HPF[u,v] = 1 / (1 + (D0/D)**n)
        
plt.imshow(HPF, cmap='gray')
plt.axis('off')
# plt.show()

Gshift = Fshift * HPF # Aplly Butterworth Low Pass Filter to frequency domain image 
G = np.fft.ifftshift(Gshift) # Return back into the corner
g = np.abs(np.fft.ifft2(G)) # Invers fourier transform

plt.imshow(g, cmap='gray')
plt.axis('off')
plt.show() # Result image sharpening with Butterworth Low Pass Filter