# Libraries
import cv2
import numpy as np
import matplotlib.pyplot as plt

# Inputan gambar
f = cv2.imread("/Users/khodijahanna/Kuliah/Kuliah (Dokumentasi coding)/semester-6/PCD/FrequencyDomainFilter/rubik.jpg",0)

# Transform Image into frequency domain using fourier's transform (by using numpy)
F = np.fft.fft2(f) # implementasi transformasi Fourier 2 dimensi
plt.imshow(np.log1p(np.abs(F)), cmap='gray') # menampilkan spektrum (menunjukkan bagian low dan high frekuensi)
plt.axis('off')
plt.show()

Fshift = np.fft.fftshift(F) #  menggeser komponen frekuensi nol dari spektrum fourier ke pusat spektrum
plt.imshow(np.log1p(np.abs(Fshift)), cmap='gray') # menampilkan spektrum (low frekuensi berada di tengah)
plt.axis('off')
plt.show()

# CREATE FILTER: Low pass filter
M,N = f.shape
H = np.zeros((M,N), dtype=np.float32)
D0 = 50
for u in range(M):
    for v in range(N):
        D = np.sqrt((u-M/2)**2 + (v-N/2)**2)
        if D <= D0:
            H[u,v] = 1
        else:
            H[u,v] = 0
            
plt.imshow(H, cmap='gray') # Menampilkan hasil filter (black = 0, white = 1) -> low frequency in the center
plt.axis('off')
plt.show()

# APPLY Ideal Low Pass Filtering
Gshift = Fshift * H # multiply image frequency domain and low pass filter was created
plt.imshow(np.log1p(np.abs(Gshift)), cmap='gray') #  see the result 
plt.axis('off')
plt.show()

# Inverse Fourier Transform to see the result 
G = np.fft.ifftshift(Gshift) # low frequency back to the corner
plt.imshow(np.log1p(np.abs(G)), cmap='gray')
plt.axis('off')
plt.show()

g = np.abs(np.fft.ifft2(G)) # inverse to see the result
plt.imshow(g, cmap='gray')
plt.axis('off')
plt.show() #RESULT LOW PASS FILTER



# CREATE FILTER: High pass filter
H = 1 - H
plt.imshow(H, cmap='gray')
plt.axis('off')
plt.show()

# APPLY Ideal High Pass Filtering
Gshift = Fshift * H
plt.imshow(np.log1p(np.abs(Gshift)), 
           cmap='gray')
plt.axis('off')
plt.show()

# Inverse Fourier Transform to see the result 
G = np.fft.ifftshift(Gshift)
plt.imshow(np.log1p(np.abs(G)), 
           cmap='gray')
plt.axis('off')
plt.show()

g = np.abs(np.fft.ifft2(G))
plt.imshow(g, cmap='gray')
plt.axis('off')
plt.show() #RESULT HIGH PASS FILTER