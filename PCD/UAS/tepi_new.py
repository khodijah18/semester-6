import cv2
import numpy as np

# Membaca citra (1)
image = cv2.imread('/Users/khodijahanna/Kuliah/Kuliah (Dokumentasi coding)/semester-6/PCD/UAS/DatasetKacang/kacang1.jpeg', 0)

# Membaca citra (2)
citra = 1 # Untuk looping path dataset
while citra <= 10:
    image = cv2.imread('/Users/khodijahanna/Kuliah/Kuliah (Dokumentasi coding)/semester-6/PCD/UAS/DatasetKacang/kacang'+ str(citra) +'.jpeg', 0)
    citra += 1

# Membaca citra (3)
while citra <= 10:
    image = cv2.imread('/Users/khodijahanna/Kuliah/Kuliah (Dokumentasi coding)/semester-6/PCD/UAS/DatasetKacang/kacang'+ str(citra) +'.jpeg', 0)
    cv2.imshow('Original Image', image)
    cv2.waitKey(0)
    citra += 1


# Operasi dilasi pada citra
kernel = np.ones((5, 5), np.uint8)
dilation = cv2.dilate(image, kernel, iterations=1)

# Mengurangi citra hasil dilasi dari citra asli
edge = cv2.absdiff(dilation, image)

# Menampilkan citra asli, hasil dilasi, dan tepi citra
cv2.imshow('Original Image', image)
cv2.imshow('Dilation', dilation)
cv2.imshow('Edge', edge)
cv2.waitKey(0)
cv2.destroyAllWindows()
