''' Langkah-langkah morfologi untuk mendeteksi tepi
1. RGB to grayscale (Kalo bisa dibikin jadi matrix 3x3. Tapi kalo terlalu besar yaudah gapapa gausa)
2. Grayscale to biner (Ini memudahkan saat roses dilasi erosi)
3. Dilasi: Ditandain kalau tidak match
4. Erosi: Ditandain kalau match
5. Tepi: Hasil Dilasi - Erosi '''
#%%
import cv2
import numpy as np # Untuk perhitungan dan matrix

#%%
# Grayscale Image
image = cv2.imread('/Users/khodijahanna/Kuliah/Kuliah (Dokumentasi coding)/semester-6/PCD/UAS/DatasetKacang/bag.jpeg', 0)
cv2.imshow('Gray Image', image)
cv2.waitKey(0)
cv2.destroyAllWindows()

# Biner Image -> Nanti bisa pakai yang udah dibuat
# Image = cv2.threshold(image, 127, 255, cv2.THRESH_BINARY)[1] 

#%%
# Terapkan operasi dilasi
dilated_image = cv2.dilate(image, np.ones((3, 3), np.uint8), iterations=1)

# Terapkan operasi erosi
eroded_image = cv2.erode(dilated_image, np.ones((3, 3), np.uint8), iterations=1)

# Hitung citra tepi
edge_image = dilated_image - eroded_image

# Tampilkan citra tepi
cv2.imshow('Citra Asli', image)
cv2.imshow('Citra Tepi', edge_image)
cv2.waitKey(0)
cv2.destroyAllWindows()

#%%
def dilasi(Image, kernel):
    dilated_image = np.zeros_like(Image)
    height, width = Image.shape
    k_height, k_width = kernel.shape
    k_center_y, k_center_x = k_height // 2, k_width // 2

    for y in range(height):
        for x in range(width):
            if Image[y, x] > 0:
                for ky in range(k_height):
                    for kx in range(k_width):
                        if kernel[ky, kx] > 0:
                            ny = y + ky - k_center_y
                            nx = x + kx - k_center_x
                            if ny >= 0 and ny < height and nx >= 0 and nx < width:
                                dilated_image[ny, nx] = 255
    return dilated_image

#%%
def erosi(Image, kernel):
    eroded_image = np.zeros_like(Image)
    height, width = Image.shape
    k_height, k_width = kernel.shape
    k_center_y, k_center_x = k_height // 2, k_width // 2

    for y in range(height):
        for x in range(width):
            all_match = True
            for ky in range(k_height):
                for kx in range(k_width):
                    if kernel[ky, kx] > 0:
                        ny = y + ky - k_center_y
                        nx = x + kx - k_center_x
                        if ny >= 0 and ny < height and nx >= 0 and nx < width:
                            if Image[ny, nx] == 0:
                                all_match = False
                                break
                        else:
                            all_match = False
                            break
            if all_match:
                eroded_image[y, x] = 255
    return eroded_image

#%%
# Buat kernel
kernel = np.array([
    [0, 1, 0],
    [1, 1, 1],
    [0, 1, 0]], dtype=np.uint8)

# Operasi dilasi, erosi, kemudian hasilnya untuk menemukan tepi citra
dilated_image = dilasi(Image, kernel)
eroded_image = erosi(Image, kernel)
edge_image = dilated_image - eroded_image

#%%
# Menampilkan citra grayscale, citra biner, citra hasil dilasi, citra hasil erosi, dan tepi citra
cv2.imshow('Gray Image', image)
print("Original Image:", image)

cv2.imshow('Binary Image', Image)
print("Original Image:", Image)

print("Dilated Image:", dilated_image)
cv2.imshow('Dilation', dilated_image)

print("Eroded Image:", eroded_image)
cv2.imshow('Eroded', eroded_image)

print("Edge Image:", edge_image)
cv2.imshow('Edge', edge_image)

cv2.waitKey(0)
cv2.destroyAllWindows()
