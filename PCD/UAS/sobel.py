#%%
import numpy as np
import cv2
from matplotlib import pyplot as plt

#%%
image = cv2.imread('/Users/khodijahanna/Kuliah/Kuliah (Dokumentasi coding)/semester-6/PCD/UAS/DatasetKacang/bag.jpeg', cv2.IMREAD_GRAYSCALE)
maskSx = np.array([[-1, 0, 1],
                   [-2, 0, 2],
                   [-1, 0, 1]])
maskSy = np.array([[1, 2, 1],
                   [0, 0, 0],
                   [-1, -2, -1]])
Jx = cv2.filter2D(image, cv2.CV_64F, maskSx)
Jy = cv2.filter2D(image, cv2.CV_64F, maskSy)
Jedge = np.sqrt(np.square(Jx) + np.square(Jy))
print("cv2.CV_64F")

# Menampilkan citra asli
plt.subplot(1, 2, 1)
plt.imshow(image, cmap='gray')
plt.title('Original')
plt.xticks([]), plt.yticks([])

# Menampilkan citra tepi hasil filter Sobel
plt.subplot(1, 2, 2)
plt.imshow(Jedge, cmap='gray')
plt.title('Sobel')
plt.xticks([]), plt.yticks([])

plt.tight_layout()
plt.show()
# %%
