import cv2

img = cv2.imread("semester-6/PCD/khodijah.png", 1) # Converting color image to grayscale image
gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY) # Showing the converted image

cv2.imshow("Converted Image", gray) 
cv2.waitKey(0)
# cv2.destroyAllWindows()