import requests
from bs4 import BeautifulSoup

# Buat permintaan ke website
url = "http://books.toscrape.com/"
response = requests.get(url)

# Parse konten HTML menggunakan BeautifulSoup
soup = BeautifulSoup(response.content, "html.parser")

# Temukan semua kontainer buku
kontainer_buku = soup.find_all("article", class_="product_pod")

# Ekstrak informasi tentang setiap buku
for buku in kontainer_buku:
    # Ekstrak judul buku
    judul = buku.h3.a["title"]
    
    # Ekstrak harga buku
    harga = buku.select_one(".price_color").get_text(strip=True)
    
    # Ekstrak ketersediaan buku
    ketersediaan = buku.select_one(".availability").get_text(strip=True)
    
    # Cetak hasilnya
    print("Judul:", judul)
    print("Harga:", harga)
    print("Ketersediaan:", ketersediaan)
    print()