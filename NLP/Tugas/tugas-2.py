from bs4 import BeautifulSoup
import requests

# dokumen teks yang akan diparsing
url = "https://id.wikipedia.org/wiki/Tanda_tanya"
response = requests.get(url)
soup = BeautifulSoup(response.content, "html.parser")

# Temukan semua kontainer
kontainer = soup.find_all("article", class_="hlist hlist-separated")

# Parsing website
title = soup.title
title_text = soup.title.text
p = soup.body.p
p_text = soup.body.p.text

# tampilkan hasil parsing
print("Judul:", title)
print("Teks Judul:", title_text)
print("Paragraf:", p)
print("Teks Paragraf:", p_text)

