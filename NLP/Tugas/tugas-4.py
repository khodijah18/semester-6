import nltk
nltk.download()
from nltk.corpus import wordnet as wn 
wn.synsets('book')

# import nltk
# nltk.download('wordnet')
# from nltk.corpus import wordnet

# # Mendapatkan sinonim dari kata "happy"
# synonyms = []
# for syn in wordnet.synsets("happy"):
#     for lemma in syn.lemmas():
#         synonyms.append(lemma.name())
# print(set(synonyms))

# # Mendapatkan arti dari kata "bank"
# meanings = []
# for syn in wordnet.synsets("bank"):
#     for lemma in syn.lemmas():
#         meanings.append(lemma.name())
# print(set(meanings))

# # Mencari hubungan antara kata "car" dan "vehicle"
# car = wordnet.synset("car.n.01")
# vehicle = wordnet.synset("vehicle.n.01")
# print(car.wup_similarity(vehicle))
