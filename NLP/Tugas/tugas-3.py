import nltk
nltk.download()

# Definisikan aturan-aturan POS tagging
patterns = [
    (r'^[A-Z].*', 'NNP'),               # Kata yang diawali huruf kapital adalah kata benda tunggal
    (r'.*ing$', 'VBG'),                 # Kata yang diakhiri -ing adalah kata kerja bentuk -ing
    (r'.*ly$', 'RB'),                   # Kata yang diakhiri -ly adalah kata keterangan
    (r'.*ould$', 'MD'),                 # Kata yang diakhiri -ould adalah kata modal
    (r'.*\'s$', 'NN$'),                 # Kata yang diakhiri -'s adalah kata benda tunggal kepunyaan
    (r'.*s$', 'NNS'),                   # Kata yang diakhiri -s adalah kata benda jamak
    (r'^-?[0-9]+(.[0-9]+)?$', 'CD'),    # Kata yang berupa angka adalah kata bilangan
    (r'.*', 'NN')                       # Default adalah kata benda tunggal
]

# Buat POS tagger dengan aturan-aturan yang sudah didefinisikan
regexp_tagger = nltk.RegexpTagger(patterns)

# Contoh kalimat
sentence = "The quick brown fox jumps over the lazy dog."

# Tokenisasi kalimat
tokens = nltk.word_tokenize(sentence)

# Lakukan POS tagging menggunakan RegexpTagger
tags = regexp_tagger.tag(tokens)

# Print hasil POS tagging
print(tags)




'''
import nltk

# Load corpus dengan POS tagging
corpus = nltk.corpus.brown.tagged_sents()

# Split data menjadi training set dan testing set
size = int(len(corpus) * 0.8)
train_sents = corpus[:size]
test_sents = corpus[size:]

# Buat unigram tagger dari training set
unigram_tagger = nltk.UnigramTagger(train_sents)

# Buat bigram tagger dengan backoff ke unigram tagger
bigram_tagger = nltk.BigramTagger(train_sents, backoff=unigram_tagger)

# Evaluasi akurasi tagger pada testing set
accuracy = bigram_tagger.evaluate(test_sents)

# Print akurasi
print("Accuracy: {:.2f}%".format(accuracy * 100))

# Contoh kalimat
sentence = "The quick brown fox jumps over the lazy dog."

# Tokenisasi kalimat
tokens = nltk.word_tokenize(sentence)

# Lakukan POS tagging menggunakan BigramTagger
tags = bigram_tagger.tag(tokens)

# Print hasil POS tagging
print(tags)
'''