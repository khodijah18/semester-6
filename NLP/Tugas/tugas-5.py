import re

# Fungsi untuk memuat kamus kata-kata dasar dari file teks
def load_dictionary(file_path):
    with open(file_path, 'r', encoding='utf-8') as file:
        dictionary = set(file.read().split())
    return dictionary

# Fungsi untuk memeriksa ejaan kata
def spell_check(word, dictionary):
    # Mengabaikan kata yang hanya terdiri dari angka atau tanda baca
    if re.match(r'^[0-9\W]+$', word):
        return True
    
    return word.lower() in dictionary

# Memuat kamus kata-kata dasar
dictionary = load_dictionary('NLP/kamus.txt')

# Contoh penggunaan
input_word = input("Masukkan kata: ")

if spell_check(input_word, dictionary):
    print("Ejaan kata benar.")
else:
    print("Ejaan kata salah.")
