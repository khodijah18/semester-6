import logging
from nltk.corpus import stopwords
from nltk.tokenize import word_tokenize
from Sastrawi.Stemmer.StemmerFactory import StemmerFactory
from telegram import Update
from telegram.ext import Updater, CommandHandler, MessageHandler, Filters, CallbackContext

# Menginisialisasi logger (opsional)
logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
                     level=logging.INFO)

# Fungsi yang akan dipanggil saat command /start diberikan
def start(update: Update, context: CallbackContext) -> None:
    update.message.reply_text('Halo! Selamat datang di Bot Perawatan Burung.')

# Fungsi untuk membersihkan teks
def clean_text(text):
    stop_words = set(stopwords.words('indonesian'))
    factory = StemmerFactory()
    stemmer = factory.create_stemmer()
    tokens = word_tokenize(text.lower())
    cleaned_text = [stemmer.stem(token) for token in tokens if token.isalnum() and token not in stop_words]
    return cleaned_text

# Fungsi untuk mendapatkan respons berdasarkan input
def get_response(message):
    cleaned_message = clean_text(message)

    # Kamus respons sesuai konteks
    context_mapping = {
        'rawat': {
            'kenari': """
Berikut adalah cara perawatan burung kenari.

1. Perhatikan Tempat Menggantung Sangkar 
2. Jemur Burung Kenari di Waktu yang Tepat Agar Terhindar dari Paparan Sinar Matahari Langsung
3. Gunakan Air Bersih dan Segar untuk Minum
4. Sediakan Pakan Biji-bijian
5. Berikan Pakan Tambahan           
6. Cara Memberi Pakan
7. Tambahkan Multivitamin dan Suplemen untuk Melengkapi Menu Makannya
8. Tambahkan Mineral
9. Menjaga Kebersihan Kandang           
10. Hati-hati Terhadap Serangan Kutu dan Tungau
11. Gunting Kuku yang Panjang
           """,
            'lovebird': """
Berikut adalah cara perawatan burung lovebird.

1. Pemberian Pakan Secara Teratur.
2. Pemberian Suplemen.
3. Pemilihan Kandang.
4. Bersihkan Kandang Secara Rutin.
5. Mengecek Kondisi Kesehatan.
6. Memandikan Burung Lovebird.
7. Menjemur Burung Lovebird.
8. Pencegahan Terhadap Hama.
9. Perhatikan Kegiatan Burung Lovebird
10. Letakkan Burung lovebird dengan Pasangannya


            
            """,
            'parkit': """
Berikut adalah cara perawatan burung parkit.
            
1. Memilih sangkar sesuai ukuran burung.
2. Memilih burung yang sehat.
3. Memberikan asupan makanan yang tepat.
4. Melakukan pembersihan kandang secara rutin.
5. Rajin memandikan burung parkit.
            
            """,
            'merpati': """
Berikut adalah cara perawatan burung merpati.
            
1. Miliki Kandang yang Layak.
2. Beri Pakan yang Sesuai.
3. Mandikan dan Jemur Burung Merpati secara Rutin.
4. Jaga Kebersihan Kandang dan Tempat Makan.
5. Perhatikan Pertumbuhan Bulu Merpati.
6. Latih Burung Kembali ke Kandang.
7. Membangun Rumah Kedua.            
            
            """
        },
        'makanan': {
            'kenari': """
Berikut adalah informasi tentang makanan burung kenari.
            
Beberapa jenis pakan tambahan yang bisa kamu berikan diantaranya seperti:
Sayuran:
- Daun selada
- Daun sawi putih atau hijau
- Bayam
- Daun ginseng
- Gambas

Buah-buahan:
- Apel
- Anggur
- Tomat
- Pepaya setengah matang
- Wortel
- Paprika

Hewani:
- Kuning telur puyuh
- Kroto
- Rayap
- Cacing
- Jangkrik
- Belalang
- Ulat bambu
- Ulat hongkong           
            """,
            'lovebird': """
Berikut adalah informasi tentang makanan burung lovebird.

- Milet merah dan milet putih
- Jagung muda
- Kangkung
- Canary
- Kwaci
- Toge
            """,
            'parkit': """
Berikut adalah informasi tentang makanan burung parkit.
            
- Biji-bijian
- Sayuran
- Buah-buahan
- Jewawut
- Milet
- Pelet
- Jagung
- Taoge
- Daun kemangi
- Kangkung.            
            
            """,
            'merpati': """
Berikut adalah informasi tentang makanan burung merpati.
            
- Biji-bijian 
- Buah dan Sayuran 
- Air
- Makanan Manusia 
- Suplemen Khusus         
            
            """
        },
        'penyakit': {
            'kenari': """
Berikut ini beberapa ciri yang menandakan kesehatan burung Kenari sedang terganggu:

1. Mata terlihat sayu, cenderung dipejamkan, dan keluar banyak cairan
2. Lubang hidung mengeluarkan ingus yang dapat menyumbat saluran pernapasannya.
3. Terlihat lemas dan malas
4. Sayap burung tampak lunglai, baik salah satu maupun kedua sisinya.
5. Bulu menjadi kusut, kusam, dan rontok
6. Sering mengangkat kaki karena ada pembengkakan pada kakinya
7. Nafsu makan hilang bahkan tidak mau memakan pakannya sama sekali
8. Suara kicauannya berubah atau tidak berkicau lagi

            """,
            'lovebird': """
Ini adalah beberapa penyakit umum pada burung lovebird dan cara penanganannya.

1. Cacingan: Burung lovebird dapat terinfeksi oleh cacing parasit dalam sistem pencernaan mereka. Gejalanya termasuk kurang nafsu makan, kelemahan, dan penurunan berat badan. Pengobatan dilakukan dengan memberikan obat cacing yang diresepkan oleh dokter hewan.
2. Kanker: Burung lovebird juga rentan terhadap penyakit kanker, terutama kanker saluran pencernaan. Gejalanya mungkin tidak terlihat pada tahap awal, tetapi biasanya meliputi penurunan nafsu makan, kelemahan, dan perubahan perilaku. Jika Anda mencurigai burung lovebird Anda terkena kanker, segera konsultasikan dengan dokter hewan untuk diagnosis dan pengobatan yang tepat.
3. Infeksi saluran pernapasan: Lovebird dapat mengalami infeksi saluran pernapasan seperti pilek dan sinusitis. Gejalanya termasuk bersin, batuk, keluarnya lendir dari hidung atau mata, dan penurunan aktivitas. Pengobatan biasanya melibatkan memberikan antibiotik yang diresepkan oleh dokter hewan.
4. Keropeng (scaly face): Penyakit ini disebabkan oleh tungau sarcoptes dan dapat menyebabkan kulit lovebird menjadi bersisik, terutama di sekitar paruh, mata, dan kaki. Pengobatan melibatkan penggunaan obat antiparasit yang dioleskan pada daerah yang terkena.
5. Malnutrisi: Burung lovebird yang tidak mendapatkan nutrisi yang cukup atau tidak seimbang dapat mengalami masalah kesehatan, termasuk penurunan berat badan, kelemahan, dan kerontokan bulu. Pastikan memberikan pakan yang seimbang dan bervariasi, termasuk biji-bijian, buah-buahan, sayuran, dan makanan tambahan yang sesuai.   
            """,
            'parkit': """
Ini adalah beberapa penyakit umum pada burung parkit dan cara penanganannya.
            
1. Psittacosis: Psittacosis, juga dikenal sebagai penyakit beo, adalah penyakit bakteri yang dapat menyerang burung parkit dan burung paruh bengkok lainnya. Gejalanya termasuk demam, kehilangan nafsu makan, nafas pendek, diare, dan masalah pernapasan. Psittacosis juga dapat mempengaruhi manusia, sehingga penting untuk segera menghubungi dokter hewan jika Anda mencurigai burung parkit Anda terkena penyakit ini.
2. Cacar burung: Cacar burung disebabkan oleh virus dan biasanya ditandai dengan munculnya benjolan atau lesi pada kulit, terutama di sekitar paruh, mata, dan kaki burung. Gejala lainnya termasuk kelemahan, penurunan nafsu makan, dan perubahan perilaku. Pengobatan biasanya melibatkan perawatan simptomatik dan isolasi burung yang sakit dari burung lain.
3. Infeksi saluran pernapasan: Burung parkit dapat mengalami infeksi saluran pernapasan seperti pilek dan sinusitis. Gejalanya termasuk bersin, batuk, keluarnya lendir dari hidung atau mata, dan penurunan aktivitas. Pengobatan biasanya melibatkan memberikan antibiotik yang diresepkan oleh dokter hewan.
4. Malnutrisi: Jika burung parkit tidak mendapatkan nutrisi yang seimbang, mereka dapat mengalami masalah kesehatan seperti penurunan berat badan, kelemahan, dan masalah pada bulu. Pastikan memberikan pakan yang sesuai, termasuk biji-bijian, buah-buahan, sayuran, dan makanan tambahan yang sesuai untuk kebutuhan nutrisi burung parkit.
5. Infeksi jamur: Jamur seperti Aspergillosis dapat menyebabkan infeksi pada saluran pernapasan dan sistem pernapasan burung parkit. Gejala yang mungkin muncul termasuk batuk, sesak napas, keluar lendir yang berlebihan, dan penurunan aktivitas. Pengobatan biasanya melibatkan pemberian obat antijamur yang diresepkan oleh dokter hewan.            
            
            """,
            'merpati': """
Ini adalah beberapa penyakit umum pada burung merpati dan cara penanganannya.

1. Trichomoniasis: Ini adalah infeksi yang disebabkan oleh parasit protozoa Trichomonas columbae. Gejala yang umum termasuk kerongkongan dan mulut bengkak, kehilangan nafsu makan, dan produksi air liur berlebihan. Penanganan melibatkan pengobatan dengan obat antiprotozoa seperti metronidazole.
2. Coccidiosis: Ini disebabkan oleh parasit protozoa dari genus Eimeria. Gejalanya meliputi diare, kehilangan nafsu makan, kelemahan, dan penurunan berat badan. Pengobatan melibatkan pemberian obat-obatan khusus yang ditujukan untuk mengatasi infeksi coccidia.
3. Poxvirus: Infeksi virus ini dapat menyebabkan pertumbuhan benjolan berkerak pada kulit, mata, dan saluran pernapasan. Pengobatan secara langsung mungkin tidak efektif, dan vaksinasi sebelumnya bisa membantu dalam pencegahan infeksi ini.
4. Aspergillosis: Penyakit ini disebabkan oleh jamur Aspergillus yang tumbuh di lingkungan yang lembab. Burung merpati terinfeksi melalui inhalasi spora jamur. Gejala termasuk kesulitan bernapas, batuk, dan penurunan berat badan. Penanganan melibatkan pengobatan dengan antijamur seperti itraconazole.
5. Salmonellosis: Ini adalah infeksi bakteri yang dapat mempengaruhi saluran pencernaan burung merpati. Gejala meliputi diare, muntah, dan kelemahan. Penting untuk menjaga kebersihan yang baik dan memberikan air minum dan pakan yang bersih. Jika infeksi terjadi, burung harus diberikan antibiotik sesuai petunjuk dokter hewan.
"""
        },
        'kebutuhan khusus': {
            'kenari': 'burung kenari memiliki beberapa kebutuhan khusus. Berikut adalah beberapa informasi tentang itu.',
            'lovebird': 'burung lovebird memiliki beberapa kebutuhan khusus. Berikut adalah beberapa informasi tentang itu.',
            'parkit': 'burung parkit memiliki beberapa kebutuhan khusus. Berikut adalah beberapa informasi tentang itu.',
            'merpati': 'burung merpati memiliki beberapa kebutuhan khusus. Berikut adalah beberapa informasi tentang itu.'
        }
    }

    # Memeriksa konteks pesan dan memberikan respons yang sesuai
    for context, value in context_mapping.items():
        cleaned_context = clean_text(context)
        for bird_type, response in value.items():
            cleaned_bird_type = clean_text(bird_type)
            if all(term in cleaned_message for term in cleaned_context) and all(term in cleaned_message for term in cleaned_bird_type):
                return response

    # Jika tidak ada konteks yang cocok, memberikan respons default
    return 'Maaf, saya tidak dapat memahami pesan Anda. Silakan coba lagi.'

# Fungsi yang akan dipanggil saat pesan diterima
def handle_message(update: Update, context: CallbackContext) -> None:
    # Mendapatkan input dari pengguna
    message = update.message.text

    # Mendapatkan respons berdasarkan input
    response = get_response(message)

    # Mengirim respons ke pengguna
    update.message.reply_text(response)

# Fungsi utama untuk menjalankan bot
def main() -> None:
    # Inisialisasi bot
    updater = Updater("6113977120:AAEqzl2FogpvtTPzqzRZQHWruSdyOaT55Tc", use_context=True)

    # Inisialisasi dispatcher untuk menangani perintah dan pesan
    dispatcher = updater.dispatcher

    # Menambahkan handler untuk perintah /start
    dispatcher.add_handler(CommandHandler("start", start))

    # Menambahkan handler untuk pesan
    dispatcher.add_handler(MessageHandler(Filters.text & ~Filters.command, handle_message))

    # Memulai bot
    updater.start_polling()

    # Membiarkan bot berjalan sampai program dihentikan
    updater.idle()

if __name__ == '__main__':
    main()
