import logging
import spacy
from telegram import Update
from telegram.ext import Updater, CommandHandler, MessageHandler, Filters, CallbackContext

# Setup logging
logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
                    level=logging.INFO)

# Memuat model bahasa Spacy untuk Bahasa Indonesia
nlp = spacy.load('xx_ent_wiki_sm')

# Data mengenai jenis burung, makanan, kesehatan, dan perawatan
data_burung = {
    'lovebird': {
        'makanan': 'Lovebird membutuhkan makanan seperti biji-bijian, buah-buahan, sayuran segar, dan pakan komersial untuk burung kecil.',
        'kesehatan': 'Pastikan lovebird Anda mendapatkan perawatan kesehatan yang baik dengan memberikannya makanan yang sehat, menyediakan air bersih, dan menjaga kebersihan kandang.',
        'perawatan': 'Lovebird perlu memiliki sangkar yang cukup besar dan nyaman untuk bergerak, juga berikan waktu untuk bermain dan berinteraksi dengan burung Anda.',
    },
    'cockatiel': {
        'makanan': 'Cockatiel perlu diberikan makanan seperti biji-bijian, buah-buahan, sayuran, dan pakan komersial untuk burung kecil.',
        'kesehatan': 'Jaga kesehatan cockatiel dengan memberikan makanan bergizi, menjaga kebersihan sangkar, serta memberikan perhatian dan kasih sayang.',
        'perawatan': 'Cockatiel perlu memiliki waktu di luar sangkar untuk terbang, berinteraksi dengan pemilik, dan menjaga kebersihan bulunya dengan mandi.',
    },
    'budgie': {
        'makanan': 'Budgie membutuhkan makanan seperti biji-bijian, buah-buahan, sayuran hijau, dan pakan komersial khusus untuk budgie.',
        'kesehatan': 'Pastikan budgie Anda tetap sehat dengan memberikan makanan yang baik, menyediakan air bersih, serta memeriksakan ke dokter hewan secara teratur.',
        'perawatan': 'Budgie senang bermain, jadi berikan mainan dan waktu untuk terbang di luar sangkar dalam ruangan yang aman.',
    }
}

# Fungsi untuk melakukan pemrosesan NLP pada input pengguna dan menghasilkan respon
def generate_response(user_input):
    doc = nlp(user_input)

    # Memeriksa entitas yang ada dalam input pengguna
    ent_labels = [ent.label_ for ent in doc.ents]

    if 'makanan' in ent_labels:
        response = data_burung[user_input.lower()]['makanan']
    elif 'kesehatan' in ent_labels:
        response = data_burung[user_input.lower()]['kesehatan']
    elif 'perawatan' in ent_labels:
        response = data_burung[user_input.lower()]['perawatan']
    else:
        response = "Maaf, saya tidak dapat memberikan informasi tentang itu. Silakan ajukan pertanyaan tentang makanan, kesehatan, atau perawatan burung peliharaan."

    return response

# Handler untuk perintah /start
def start(update: Update, context: CallbackContext):
    context.bot.send_message(chat_id=update.effective_chat.id, text="Halo! Selamat datang di Chatbot Merawat Burung Peliharaan. Silakan ajukan pertanyaan atau pilih topik yang ingin Anda ketahui.")

# Handler untuk perintah /help
def help_command(update: Update, context: CallbackContext):
    context.bot.send_message(chat_id=update.effective_chat.id, text="Anda dapat mengajukan pertanyaan seputar merawat burung peliharaan. Beberapa topik yang dapat Anda tanyakan meliputi makanan, kesehatan, dan perawatan.")

# Handler untuk pesan yang diterima
def handle_message(update: Update, context: CallbackContext):
    user_input = update.message.text

    # Menghasilkan respon kontekstual menggunakan pemrosesan NLP
    response = generate_response(user_input)

    context.bot.send_message(chat_id=update.effective_chat.id, text=response)

# Fungsi utama
def main():
    # Inisialisasi objek Updater dengan token bot Anda
    updater = Updater(token='6043684150:AAG2Z62Ck8Q-fA3yZaw1bskNXR_wBYCbfS8', use_context=True)
    dispatcher = updater.dispatcher

    # Menambahkan handler ke dispatcher
    dispatcher.add_handler(CommandHandler('start', start))
    dispatcher.add_handler(CommandHandler('help', help_command))
    dispatcher.add_handler(MessageHandler(Filters.text & (~Filters.command), handle_message))

    # Memulai bot
    updater.start_polling()

    # Menjaga program berjalan hingga dihentikan
    updater.idle()

if __name__ == '__main__':
    main()
