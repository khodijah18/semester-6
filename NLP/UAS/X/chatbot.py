import logging
import nltk
nltk.download('stopwords')
nltk.download('punkt')
nltk.download('wordnet')
from nltk.corpus import stopwords
from nltk.tokenize import word_tokenize
from Sastrawi.Stemmer.StemmerFactory import StemmerFactory
from telegram import Update
from telegram.ext import Updater, CommandHandler, MessageHandler, Filters, CallbackContext

# Menginisialisasi logger (opsional)
logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
                     level=logging.INFO)

# Fungsi yang akan dipanggil saat command /start diberikan
def start(update: Update, context: CallbackContext) -> None:
    update.message.reply_text('Halo! Selamat datang di Bot Perawatan Burung.')

# Fungsi untuk membersihkan teks
def clean_text(text):
    stop_words = set(stopwords.words('indonesian'))
    factory = StemmerFactory()
    stemmer = factory.create_stemmer()
    tokens = word_tokenize(text.lower())
    cleaned_text = [stemmer.stem(token) for token in tokens if token.isalnum() and token not in stop_words]
    return cleaned_text

# Fungsi untuk mendapatkan respons berdasarkan input
def get_response(message):
    cleaned_message = clean_text(message)

    # Kamus respons sesuai konteks
    context_mapping = {
        'perawatan': {
            'kenari': 'Berikut adalah cara perawatan burung kenari.',
            'lovebird': 'Berikut adalah cara perawatan burung lovebird.',
            'parkit': 'Berikut adalah cara perawatan burung parkit.',
            'merpati': 'Berikut adalah cara perawatan burung merpati.'
        },
        'makanan': {
            'kenari': 'Berikut adalah informasi tentang makanan burung kenari.',
            'lovebird': 'Berikut adalah informasi tentang makanan burung lovebird.',
            'parkit': 'Berikut adalah informasi tentang makanan burung parkit.',
            'merpati': 'Berikut adalah informasi tentang makanan burung merpati.'
        },
        'penyakit': {
            'kenari': 'Ini adalah beberapa penyakit umum pada burung kenari dan cara penanganannya.',
            'lovebird': 'Ini adalah beberapa penyakit umum pada burung lovebird dan cara penanganannya.',
            'parkit': 'Ini adalah beberapa penyakit umum pada burung parkit dan cara penanganannya.',
            'merpati': 'Ini adalah beberapa penyakit umum pada burung merpati dan cara penanganannya.'
        },
        'kebutuhan khusus': {
            'kenari': 'burung kenari memiliki beberapa kebutuhan khusus. Berikut adalah beberapa informasi tentang itu.',
            'lovebird': 'burung lovebird memiliki beberapa kebutuhan khusus. Berikut adalah beberapa informasi tentang itu.',
            'parkit': 'burung parkit memiliki beberapa kebutuhan khusus. Berikut adalah beberapa informasi tentang itu.',
            'merpati': 'burung merpati memiliki beberapa kebutuhan khusus. Berikut adalah beberapa informasi tentang itu.'
        }
    }

    # Memeriksa konteks pesan dan memberikan respons yang sesuai
    for context, value in context_mapping.items():
        cleaned_context = clean_text(context)
        for bird_type, response in value.items():
            cleaned_bird_type = clean_text(bird_type)
            if all(term in cleaned_message for term in cleaned_context) and all(term in cleaned_message for term in cleaned_bird_type):
                return response

    # Jika tidak ada konteks yang cocok, memberikan respons default
    return 'Maaf, saya tidak dapat memahami pesan Anda. Silakan coba lagi.'

# Fungsi yang akan dipanggil saat pesan diterima
def handle_message(update: Update, context: CallbackContext) -> None:
    # Mendapatkan input dari pengguna
    message = update.message.text

    # Mendapatkan respons berdasarkan input
    response = get_response(message)

    # Mengirim respons ke pengguna
    update.message.reply_text(response)

# Fungsi utama untuk menjalankan bot
def main() -> None:
    # Inisialisasi bot
    updater = Updater("6113977120:AAEqzl2FogpvtTPzqzRZQHWruSdyOaT55Tc", use_context=True)

    # Inisialisasi dispatcher untuk menangani perintah dan pesan
    dispatcher = updater.dispatcher

    # Menambahkan handler untuk perintah /start
    dispatcher.add_handler(CommandHandler("start", start))

    # Menambahkan handler untuk pesan
    dispatcher.add_handler(MessageHandler(Filters.text & ~Filters.command, handle_message))

    # Memulai bot
    updater.start_polling()

    # Membiarkan bot berjalan sampai program dihentikan
    updater.idle()

if __name__ == '__main__':
    main()

#6043684150:AAG2Z62Ck8Q-fA3yZaw1bskNXR_wBYCbfS8