import logging
from telegram import Update, ReplyKeyboardMarkup, ReplyKeyboardRemove
from telegram.ext import Updater, CommandHandler, MessageHandler, filters, CallbackContext

# Setup logging
logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
                    level=logging.INFO)

# Data mengenai jenis burung, makanan, kesehatan, dan perawatan
data_burung = {
    'lovebird': {
        'makanan': 'Lovebird membutuhkan makanan seperti biji-bijian, buah-buahan, sayuran segar, dan pakan komersial untuk burung kecil.',
        'kesehatan': 'Pastikan lovebird Anda mendapatkan perawatan kesehatan yang baik dengan memberikannya makanan yang sehat, menyediakan air bersih, dan menjaga kebersihan kandang.',
        'perawatan': 'Lovebird perlu memiliki sangkar yang cukup besar dan nyaman untuk bergerak, juga berikan waktu untuk bermain dan berinteraksi dengan burung Anda.',
    },
    'cockatiel': {
        'makanan': 'Cockatiel perlu diberikan makanan seperti biji-bijian, buah-buahan, sayuran, dan pakan komersial untuk burung kecil.',
        'kesehatan': 'Jaga kesehatan cockatiel dengan memberikan makanan bergizi, menjaga kebersihan sangkar, serta memberikan perhatian dan kasih sayang.',
        'perawatan': 'Cockatiel perlu memiliki waktu di luar sangkar untuk terbang, berinteraksi dengan pemilik, dan menjaga kebersihan bulunya dengan mandi.',
    },
    'budgie': {
        'makanan': 'Budgie membutuhkan makanan seperti biji-bijian, buah-buahan, sayuran hijau, dan pakan komersial khusus untuk budgie.',
        'kesehatan': 'Pastikan budgie Anda tetap sehat dengan memberikan makanan yang baik, menyediakan air bersih, serta memeriksakan ke dokter hewan secara teratur.',
        'perawatan': 'Budgie senang bermain, jadi berikan mainan dan waktu untuk terbang di luar sangkar dalam ruangan yang aman.',
    }
}

# Fungsi untuk mendapatkan informasi burung
def get_bird_info(bird_name):
    bird_name = bird_name.lower()
    if bird_name in data_burung:
        info = data_burung[bird_name]
        return f'Informasi mengenai {bird_name.capitalize()}:\n\nMakanan: {info["makanan"]}\nKesehatan: {info["kesehatan"]}\nPerawatan: {info["perawatan"]}'
    else:
        return 'Maaf, saya tidak memiliki informasi tentang burung tersebut.'

# Handler untuk command /start
def start(update: Update, context: CallbackContext):
    update.message.reply_text('Selamat datang di Chatbot Informasi Burung!')

# Handler untuk command /help
def help_command(update: Update, context: CallbackContext):
    update.message.reply_text('Silakan masukkan nama burung (lovebird/cockatiel/budgie) untuk mendapatkan informasi. Ketik /exit untuk keluar.')

# Handler untuk menerima pesan dari pengguna
def handle_message(update: Update, context: CallbackContext):
    user_input = update.message.text

    if user_input.lower() == '/exit':
        update.message.reply_text('Terima kasih telah menggunakan Chatbot Informasi Burung. Sampai jumpa lagi!')
        return

    info = get_bird_info(user_input)
    update.message.reply_text(info)

# Fungsi utama
def main():
    # Inisialisasi bot
    updater = Updater('6113977120:AAEqzl2FogpvtTPzqzRZQHWruSdyOaT55Tc', use_context=True)

    # Daftarkan handler
    dispatcher = updater.dispatcher
    dispatcher.add_handler(CommandHandler('start', start))
    dispatcher.add_handler(CommandHandler('help', help_command))
    dispatcher.add_handler(MessageHandler(filters.text & ~filters.command, handle_message))

    # Jalankan bot
    updater.start_polling()
    updater.idle()

if __name__ == '__main__':
    main()
